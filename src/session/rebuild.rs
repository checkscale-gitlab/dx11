// use std::fs;
use std::path::Path;
use std::process::{exit, Command, Stdio};

#[path = "../lib/mod.rs"]
mod lib;
use lib::common::{confirm_action, ensure_session_paths, get_paths_for_session, is_valid_name};
use lib::config::{read_config, write_config};

pub fn rebuild_session(name: &str, use_tor: bool) {
    if !is_valid_name(name.to_string()) {
        println!("The session name \"{}\" is not valid", name);
        exit(1);
    }

    let paths = get_paths_for_session(name.to_string());
    let cached_docker_file = &paths.docker_folder.join("Dockerfile");
    let cached_docker_file_path = Path::new(cached_docker_file);

    if !paths.base_path.exists() {
        println!("Error: The session path does not exist");
        exit(1);
    } else if !paths.docker_folder.exists() {
        println!("Error: The session build data does not exist");
        exit(1);
    } else if !cached_docker_file_path.exists() {
        println!("Error: The session build data does not exist");
        exit(1);
    }

    println!("This will overwrite the current Docker image and build it from scratch.\n");
    if confirm_action(
        "Do you want to keep a backup of the current session image?",
        true,
    ) {
        let docker_image_name = format!("dx11-{}:latest", name);
        let docker_image_name_bak = format!("dx11-{}:bak", name);

        let build_command = "sudo";
        let build_command_args = ["docker", "tag", &docker_image_name, &docker_image_name_bak];

        let mut docker_build = Command::new(build_command);

        docker_build.stdin(Stdio::null());
        docker_build.stdout(Stdio::inherit());
        docker_build.stderr(Stdio::inherit());

        for arg in build_command_args.iter() {
            docker_build.arg(arg);
        }
        match docker_build.output() {
            Err(err) => {
                println!("Error: Failed to make a copy of the image ({})", err);
                exit(1);
            }
            _ => {}
        };
    }

    if !paths.config_file.exists() {
        println!("Error: The session config file does not exist");
        exit(1);
    }

    if !confirm_action("Do you want to rebuild the Docker image?", false) {
        exit(0);
    }

    let mut config = match read_config(&paths.config_file) {
        Err(err) => {
            println!("Error: Could not read the config file ({})", err);
            exit(1);
        }
        Ok(value) => value,
    };
    config.useTor = use_tor;
    match write_config(&config, &paths.config_file) {
        Err(err) => {
            println!("Error: \"{}\" cannot update the settings", err);
            exit(1);
        }
        _ => {}
    }

    match ensure_session_paths(name.to_string()) {
        Err(err) => {
            println!("Error: \"{}\" unable to access the session paths", err);
            exit(1);
        }
        _ => {}
    }

    // TODO: CHECK ENVIRONMENT AND DEPENDENCIES

    let docker_image_name = format!("dx11-{}", name);
    let cached_docker_file_path = paths.docker_folder.join("Dockerfile");
    let build_command = "sudo";
    let build_command_args = [
        "docker",
        "build",
        "--no-cache",
        "-t",
        &docker_image_name,
        "-f",
        &cached_docker_file_path.display().to_string(),
        &paths.docker_folder.display().to_string(),
    ];

    println!("Rebuilding the Docker image {}", &docker_image_name);

    let mut docker_build = Command::new(build_command);

    docker_build.stdin(Stdio::null());
    docker_build.stdout(Stdio::inherit());
    docker_build.stderr(Stdio::inherit());

    for arg in build_command_args.iter() {
        docker_build.arg(arg);
    }
    match docker_build.output() {
        Err(err) => {
            println!("Error: Failed to rebuild the image ({})", err);
            exit(1);
        }
        Ok(_) => println!("Done"),
    };
}
