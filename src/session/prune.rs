use std::process::{Command, Stdio};

pub fn prune_images() {
    let mut docker_prune = Command::new("sudo");
    docker_prune.arg("docker");
    docker_prune.arg("image");
    docker_prune.arg("prune");

    docker_prune.stdin(Stdio::inherit());
    docker_prune.stdout(Stdio::inherit());
    docker_prune.stderr(Stdio::inherit());

    match docker_prune.output() {
        Err(err) => {
            println!("Error: Failed to clean the unused Docker images ({})", err);
            std::process::exit(1);
        }
        Ok(_) => println!("Done"),
    };
}
