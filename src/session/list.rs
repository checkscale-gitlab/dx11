use regex::Regex;
use std::process::Command;

#[path = "../lib/mod.rs"]
mod lib;
use lib::common::get_dx11_data_path;

pub fn list_sessions() {
    let base_path = get_dx11_data_path();
    let mut ls_sessions = Command::new("ls");
    ls_sessions.arg(base_path);
    let ls_output = ls_sessions.output().expect("Failed to list the sessions");

    let str_sessions = String::from_utf8(ls_output.stdout).unwrap_or("".to_string());

    println!("Available DX11 sessions:");

    let re = Regex::new(r"\n").unwrap();
    let result2 = re.replace_all(&str_sessions.trim(), "\n - ");
    println!(" - {}", result2);
}
