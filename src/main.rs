extern crate clap;
use clap::{App, AppSettings, Arg, SubCommand};

mod lib;

mod session;
use session::delete::delete_session;
use session::list::list_sessions;
use session::new::new_session;
use session::prune::prune_images;
use session::rebuild::rebuild_session;

// Examples:
//
// dx11 new personal Dockerfile
// dx11 new dev Dockerfile --tor --encrypted
// dx11 run personal
// dx11 exec personal <cmd>
// dx11 list
// dx11 rebuild personal   (rerun docker)
// dx11 delete dev
// dx11 prune

fn main() {
   #[cfg(not(target_os = "linux"))]
   panic!("dx11 only runs on Linux");

   let matches = App::new("DX11")
      .version("1.0")
      .author("Brick Pop <itsbrickpop@gmail.com>")
      .about("CLI manager for GUI Linux sessions")
      .setting(AppSettings::ArgRequiredElseHelp)
      .subcommand(
         SubCommand::with_name("new")
            .about("Builds a new Linux session or updates an existing one")
            .arg(
               Arg::with_name("session-name")
                  .help("The name that you will use to run the session in the future")
                  .required(true),
            )
            .arg(
               Arg::with_name("docker-file")
                  .help("The Docker file with instructions to build your environment")
                  .required(true),
            )
            .arg(
               Arg::with_name("encrypt")
                  .short("e")
                  .long("encrypt")
                  .help("Enables encryption for the new session (unless it already exists)"),
            )
            .arg(
               Arg::with_name("tor")
                  .short("t")
                  .long("tor")
                  .help("Uses TOR networking instead"),
            ),
      )
      .subcommand(
         SubCommand::with_name("run")
            .about("Starts an X11 session with your environment and data")
            .arg(
               Arg::with_name("session-name")
                  .help("The name of the session to run")
                  .required(true),
            ),
      )
      .subcommand(
         SubCommand::with_name("exec")
            .about("Runs a command within an already running session as root")
            .arg(
               Arg::with_name("session-name")
                  .help("The name of your session")
                  .required(true),
            )
            .arg(
               Arg::with_name("commands")
                  .multiple(true)
                  .help("The command to run")
                  .required(true),
            ),
      )
      .subcommand(SubCommand::with_name("list").about("Display the available sessions to run"))
      .subcommand(
         SubCommand::with_name("rebuild")
            .about("Forces a Docker rebuild of the existing session")
            .arg(
               Arg::with_name("session-name")
                  .help("The name of the session to rebuild")
                  .required(true),
            ),
      )
      .subcommand(
         SubCommand::with_name("delete")
            .about("Removes the settings, data and Docker image of a session")
            .arg(
               Arg::with_name("session-name")
                  .help("The name of the session to remove")
                  .required(true),
            ),
      )
      .subcommand(SubCommand::with_name("prune").about("Clean unused Docker images"))
      .get_matches();

   match matches.subcommand {
      None => {
         println!("{}", matches.usage.unwrap());
         return;
      }
      _ => {}
   }

   let cmd = matches.subcommand.unwrap();
   let cmd_name = cmd.name.as_str();

   match cmd_name {
      "new" => {
         new_session(
            cmd.matches.value_of("session-name").unwrap(),
            cmd.matches.value_of("docker-file").unwrap(),
            cmd.matches.is_present("encrypt"),
            cmd.matches.is_present("tor"),
         );
      }
      "run" => {
         println!("RUN");
         println!("{}", cmd.matches.value_of("session-name").unwrap());
      }
      "exec" => {
         println!("RUN EXEC");
         println!("{}", cmd.matches.value_of("session-name").unwrap());
         println!("{:?}", cmd.matches.args.get("commands").unwrap().vals);
      }
      "list" => {
         list_sessions();
      }
      "rebuild" => {
         rebuild_session(
            cmd.matches.value_of("session-name").unwrap(),
            cmd.matches.is_present("tor"),
         );
      }
      "delete" => {
         delete_session(cmd.matches.value_of("session-name").unwrap().to_string());
      }
      "prune" => {
         prune_images();
      }
      _ => {
         println!("ELSE");
      }
   }
}
